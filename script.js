//class Person {
//	constructor(age, name) {
//		this.name = name;
//		this.age = age;
//	}
//	descpibe() {
//		return `${this.name},${this.age} years old`
//	}
//}


//const jack = new Person(25, "Jach")
//const jill = new Person(35, "Jill")

//console.log(jack.descpibe())
//console.log(jill.descpibe())

class Person {
	constructor(name, age, favoriteToy) {
		[this.name, this.age, this.favoriteToy] = [name, age, favoriteToy]
	}
}

class Baby extends Person {
	constructor(name, age, favoriteToy) {
		super(name, age);
		this.favoriteToy = favoriteToy;
	}

	play() {
		console.log(`${this.name} is ${this.age} years old, and likes play ${this.favoriteToy}`)
	}
}

const baby = new Baby("Botir", 2, "ball");


console.log(baby.play());